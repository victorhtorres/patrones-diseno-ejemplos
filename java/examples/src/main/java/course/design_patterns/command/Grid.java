package course.design_patterns.command;

import java.util.ArrayList;


/**
 * Invocador.
 *
 * Ver la clase Form para más detalles.
 *
 * @author Manuel Zapata
 */
class Grid {

    private ArrayList<Product> products;
    private Command command;

    public Grid() {
        products = new ArrayList<>();

        //Añadamos algunos datos de prueba
        products.add(new Product(101, "Mesa vintage"));
        products.add(new Product(102, "Silla chic"));
        products.add(new Product(103, "Lámpara clásica"));
    }

    public void setCommand(Command command){
        this.command = command;
    }

    void update() {
        System.out.println("Desde el método update del formulario");
        /*
         * Cada invocador sabe en qué momento y de qué forma utiliza el comando.
         *
         * Por ejemplo, aquí se invoca varias veces el comando: una por cada objeto de la lista.
         * */
        for(Product product: products) {
           command.execute(product);
        }

    }

}
