package course.design_patterns.command;

/**
 * Client (cliente).
 *
 * Es la clase coordinadora, que se encarga de pasar las dependencias a las demás.
 *
 * @author Manuel Zapata
 */
class CommandClient {

    public static void main(String[] args) {

        ProductService productService = new ProductService();
        //Se indica al comando cuál es el receptor que debe usar el comando.
        Command saveCommand = new SaveCommand(productService);

        Form form = new Form();
        //Se establece el comando para el invocador.
        form.setCommand(saveCommand);

        Grid productGrid = new Grid();
        //Se establece el comando para el otro invocador. Se puede usar el mismo comando para varios invocadores.
        productGrid.setCommand(saveCommand);


        //En algún punto de la ejecución, se llamará al método save() del formulario. Ahí se disparará el llamado al
        //comando.
        form.save();

        //De igual forma, se llamará en algún momento al método update() de la grilla
        productGrid.update();

    }

}
