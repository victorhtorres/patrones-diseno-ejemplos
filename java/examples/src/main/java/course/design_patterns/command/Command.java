package course.design_patterns.command;

/**
 * Comando (command).
 *
 * Interfaz que deben implementar todos los comandos.
 *
 * @author Manuel Zapata
 */

interface Command {

    /**
     * El método que contiene la lógica que debe ejecutar el comando.
     *
     * Normalmente, en algún punto de la ejecución, el comando deberá llamar el receptor de la acción,
     * quien es el objeto encargado de completar la solicitud.
     *
     * */
    void execute(Product product);

}
