package course.design_patterns.observer;

/**
 * Esta clase no hace parte de la estructura del patrón, pero es importante porque nos permite ver como se configuran
 * los objetos.
 *
 * @author Manuel Zapata
 */
class ObserverClient {

    public static void main(String[] args) {

        //Creamos el objeto que escuchará las notificaciones.
        Observer observer = new ConsoleObserver();

        //Creamos el objeto que generará las notificaciones.
        GeologicalService subject = new GeologicalService();

        //Para que el observador sea notificado, debe esta suscrito al sujeto.
        subject.subscribe(observer);
        //Llamando el siguiente método, simulamos que el terremoto ocurrió.
        subject.tremble();

        System.out.println();

        //Se desuscribe el observador cuando ya no le interese escuchar.
        subject.unsubscribe(observer);
        //Simulemos que otro evento ocurrió.
        subject.tremble();


    }

}
