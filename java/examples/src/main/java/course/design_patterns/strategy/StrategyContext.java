package course.design_patterns.strategy;

/**
 * Contexto (context).
 *
 * Es la clase contra la cual los clientes interactuan.
 *
 * Debe tener una referencia a la estrategia que se está usando en el momento.
 *
 * @author Manuel Zapata
 */
class StrategyContext {

    //Referencia a la estrategia actual. No sabe qué estrategia concreta se está usando.
    private EncryptionStrategy strategy;

    /**
     * Cuando se crea el contexto, es necesario indicar qué estrategia se usará inicialmente.
     * */
    public StrategyContext(EncryptionStrategy strategy) {
        setStrategy(strategy);
    }

    public byte[] encryptText(String text) {
        //El contexto delega el trabajo a la estrategia.
        byte[] result = strategy.encrypt(text);
        return result;
    }

    public String decryptText(byte[] text) {
        //El contexto delega el trabajo a la estrategia.
        String result = strategy.decrypt(text);
        return result;
    }

    /**
     * La estrategia puede ser cambiada en cualquier momento, a través de este método.
     * */
    public void setStrategy(EncryptionStrategy strategy) {
        this.strategy = strategy;
        this.strategy.init();
    }



}
