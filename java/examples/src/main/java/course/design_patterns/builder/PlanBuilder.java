package course.design_patterns.builder;

import course.design_patterns.builder.entities.AccommodationType;

import java.time.LocalDate;

/**
 * Constructor (builder).
 *
 * Esta interfaz debe ser implementada por todos los constructores.
 *
 * Cada unos los métodos indica los distintos pasos en la construcción del objeto.
 *
 * @author Manuel Zapata
 * */
interface PlanBuilder {

    void definePeriod(LocalDate startDate, LocalDate endDate);

    void defineStudyProgram(int programId, int universityId);

    boolean defineAccommodation(AccommodationType type, LocalDate startDate, LocalDate endDate);

    /**
     * Este método es necesario para asegurarnos que el constructor empieza "limpio" para crear un nuevo objeto. Es
     * decir, que no van a quedar rastros de configuraciones o de productos anteriores.
     * */
    void reset();

}
