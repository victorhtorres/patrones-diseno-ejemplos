package course.design_patterns.builder;

import course.design_patterns.builder.entities.Accommodation;
import course.design_patterns.builder.entities.AccommodationType;
import course.design_patterns.builder.entities.StudyProgram;

import java.time.LocalDate;

/**
 * Constructor concreto (concrete builder).
 *
 * Representa un constructor especializado en la creación de cierto producto. Implementa la interfaz
 * Builder (PlanBuilder en este caso).
 *
 * Ver clase InternationalPlanBuilder para más detalles.
 *
 * @author Manuel Zapata
 * */
class LocalPlanBuilder implements PlanBuilder {

    private LocalPlan plan;

    public LocalPlanBuilder() {
        reset();
    }

    @Override
    public void definePeriod(LocalDate startDate, LocalDate endDate) {
        plan.setDates(startDate, endDate);
    }

    @Override
    public void defineStudyProgram(int programId, int universityId) {
        StudyProgram program = new StudyProgram(programId, universityId);
        plan.setStudyProgram(program);
    }

    @Override
    public boolean defineAccommodation(AccommodationType type, LocalDate startDate, LocalDate endDate) {
        Accommodation accommodation = new Accommodation(type, startDate, endDate);
        plan.setAccommodation(accommodation);
        return true;
    }

    @Override
    public void reset() {
        plan = new LocalPlan();
    }

    public LocalPlan getPlan() {
        LocalPlan builtPlan = plan;
        reset();
        return builtPlan;
    }
}
