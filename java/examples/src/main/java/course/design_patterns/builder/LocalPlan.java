package course.design_patterns.builder;

import course.design_patterns.builder.entities.Accommodation;
import course.design_patterns.builder.entities.StudyProgram;

import java.time.LocalDate;

/**
 * Producto.
 *
 * Representa uno de los productos que puede crear un constructor (builder).
 *
 * A diferencia de otros patrones, este producto no tiene que pertenecer a ninguna jerarquía de clases.
 *
 * La estructura del producto depende totalmente del sistema donde se esté usando el patrón.
 *
 * @author Manuel Zapata
 * */
class LocalPlan {

    private LocalDate startDate;
    private LocalDate endDate;
    private StudyProgram studyProgram;
    private Accommodation accommodation;

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setDates(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public StudyProgram getStudyProgram() {
        return studyProgram;
    }

    public void setStudyProgram(StudyProgram studyProgram) {
        this.studyProgram = studyProgram;
    }

    public Accommodation getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }

    public String toString() {
        return "Plan local desde " + startDate + " hasta " + endDate;
    }

}
