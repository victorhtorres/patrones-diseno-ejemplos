package course.design_patterns.builder.entities;

import java.time.LocalDate;

/**
 * Esta clase no hace parte de la estructura del patrón, pero nos ayuda a darle forma al producto que deben crear los
 * constructores.
 *
 * @author Manuel Zapata
 */
public class Accommodation {

    private AccommodationType accommodationType;
    private LocalDate startDate;
    private LocalDate endDate;

    public Accommodation(AccommodationType accommodationType, LocalDate startDate, LocalDate endDate) {
        this.accommodationType = accommodationType;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
