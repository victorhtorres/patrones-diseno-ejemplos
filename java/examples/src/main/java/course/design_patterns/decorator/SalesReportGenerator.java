package course.design_patterns.decorator;

import java.util.List;

/**
 * Componente (component).
 *
 * Esta interfaz inicia la jerarquía tanto para los componentes concretos como para los decoradores.
 *
 * @author Manuel Zapata
 * */
interface SalesReportGenerator {

    /**
     * La funcionalidad en este método es la que será aumentada a través del uso de decoradores.
     * */
    String generate(List<ReportItem> items);

}
