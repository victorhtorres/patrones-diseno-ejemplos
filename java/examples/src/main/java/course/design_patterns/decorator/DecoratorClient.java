package course.design_patterns.decorator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase no hace parte de la estructura básica del patrón, pero es importante porque nos permite ver cómo el
 * código es independiente del generador de reportes que estemos usando.
 *
 * @author Manuel Zapata
 */
class DecoratorClient {

    public void execute(SalesReportGenerator generator) {

        List<ReportItem> items = new ArrayList<>();

        //Creamos algunos datos de prueba.
        LocalDate now = LocalDate.now();
        items.add(new ReportItem(100, "Maria Rodriguez", now.minusDays(1), 100));
        items.add(new ReportItem(101, "Juan Lopez", now.minusDays(2), 200));
        items.add(new ReportItem(102, "Pedro Perez", now.minusDays(3), 300));

        //Invocamos el decorador.
        String filePath = generator.generate(items);
        System.out.println(filePath);

    }

}
