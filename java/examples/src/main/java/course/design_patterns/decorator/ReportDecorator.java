package course.design_patterns.decorator;

import java.util.List;

/**
 * Decorador (decorator).
 *
 * Esta es la clase a partir de la cual tienen que heredar los decoradores.
 *
 * Es abstracta para que no sea posible crear objetos a partir de esta.
 *
 * Debe implementar la interfaz que representa el componente. Para nuestro ejemplo, SalesReportGenerator.
 *
 * @author Manuel Zapata
 * */
abstract class ReportDecorator implements SalesReportGenerator{

    //Este es el componente a decorar. Podría ser un componente concreto u otro decorador.
    private SalesReportGenerator targetGenerator;

    /**
     * Inyectamos la dependencia con el componente a decorar.
     * **/
    public ReportDecorator(SalesReportGenerator generator) {
        targetGenerator = generator;
    }

    /**
     * Esta es solo una implementación por defecto. Los decoradores concretos son quienes deben implementar este
     * método apropiadamente.
     * */
    public String generate(List<ReportItem> items){
        return targetGenerator.generate(items);
    }

    public SalesReportGenerator getTargetGenerator() {
        return targetGenerator;
    }
}
