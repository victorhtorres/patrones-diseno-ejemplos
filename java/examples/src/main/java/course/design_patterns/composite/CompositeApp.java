package course.design_patterns.composite;

/**
 * Esta clase no hace parte de la estructura del patrón, pero es importante porque nos permite ver como se configuran
 * los objetos.
 *
 * @author Manuel Zapata
 */
public class CompositeApp {

    public static void main(String[] args) {

        Manager manager = new Manager("Steven", "Senior Manager");

        Manager anotherManager = new Manager("Nicole", "Mid Manager");

        anotherManager.add(new Engineer("Carlos", "Senior Software Engineer"));
        anotherManager.add(new Engineer("Andrea", "Junior Software Engineer"));

        //Dado que los managers también son componentes, es posible que tengamos varios niveles de managers.
        manager.add(anotherManager);

        //Asignamos un proyecto a toda la jerarquía.
        manager.assignProject("Migracion a la nube");

        //Para el cliente es transparente si recibe una hoja o un objeto compuesto.
        Department department = new Department(manager);
        department.printMembers();

    }

}
