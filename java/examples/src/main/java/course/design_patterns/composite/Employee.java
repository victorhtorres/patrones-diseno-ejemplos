package course.design_patterns.composite;

/**
 * Componente (component).
 *
 * Clase abstracta que inicia la jerarquía tanto de hojas como de objetos compuestos. También podría ser
 * una interfaz.
 *
 * @author Manuel Zapata
 */
abstract class Employee {

    //Los siguientes atributos son específicos al ejemplo, no al patrón.
    private String name;
    private String positionName;
    private String currentProject;

    public Employee(String name, String positionName) {
        this.name = name;
        this.positionName = positionName;
    }

    /**
     * Este método define uno de los comportamientos que deberían tener las clases hijas.
     * */
    abstract void print();

    /**
     * Este método define uno de los comportamientos que deberían tener las clases hijas.
     * */
    abstract void assignProject(String projectName);

    /**
     * Permite agregar un componente como hijo del componente actual (this). El hijo podría ser tanto un componente
     * concreto como un objeto compuesto.
     * */
    abstract void add(Employee employee);

    /**
     * Remueve un componente como hijo del componente actual (this).
     * */
    abstract void remove(Employee employee);

    /**
     * Obtiene el componente en la posición indicada.
     * */
    abstract Employee getEmployee(int index);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getCurrentProject() {
        return currentProject;
    }

    public void setCurrentProject(String currentProject) {
        this.currentProject = currentProject;
    }
}
