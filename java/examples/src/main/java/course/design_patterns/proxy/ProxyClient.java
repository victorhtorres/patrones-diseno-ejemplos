package course.design_patterns.proxy;

/**
 *
 * Cliente (client).
 *
 * Representa el cliente que utilizará el proxy.
 *
 * @author Manuel Zapata
 */
class ProxyClient {

    //El sujeto que utilizará el cliente.
    private UserService userService;


    /**
     * Aquí estamos inyectando una dependencia de tipo UserService (la interfaz).
     *
     * Esto hace que para el cliente sea transparente si usa el sujeto real (BaseUserService) o el proxy
     * (UserServiceLogger).
     * */
    public ProxyClient(UserService userService) {
        this.userService = userService;
    }

    public void saveUser(String username) {

        //Ejecutar lógica de negocio antes de llamar el servicio

        userService.save(username);

        //Ejecutar lógica de negocio después de llamar el servicio
    }

}
