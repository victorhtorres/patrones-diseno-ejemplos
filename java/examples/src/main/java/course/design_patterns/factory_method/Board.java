package course.design_patterns.factory_method;

/**
 * Creador (creator).
 *
 * Clase abstracta que inicia la jerarquía de clases que van a definir el método fábrica. También podría ser una
 * interfaz.
 *
 * @author Manuel Zapata
 */
abstract class Board {

    private Game game;
    private String[] players;

    public Board(String[] players) {
        this.players = players;
    }

    /**
     * Este es nuestro método fábrica.
     *
     * Es abstracto porque las clases hijas son las que tienen la responsabilidad de definir qué y cómo se deben
     * crear cada uno de los productos.
     *
     * */
    abstract Game createGame();

    public void initialize() {

        /*
        * En algún punto de la ejecución del creador, se debe llamar al método fábrica para obtener una instancia.
        * */
        this.game = createGame();
        this.game.start();
    }

    public String[] getPlayers() {
        return players;
    }

}
