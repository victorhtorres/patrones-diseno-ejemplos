package course.design_patterns.factory_method.simple_factory;

/**
 * Fábrica sencilla (simple factory).
 *
 * Clase que contendrá el método encargado de la creación de absolutamente todos los objetos de la jerarquía.
 *
 * @author Manuel Zapata
 */
class SimpleFactory {

    public static Game createGame(String type) {

        Game game = null;

        switch(type) {
            case "chess":
                game = new ChessGame();
                break;
            case "solitaire":
                game = new SolitaireGame();
                break;
        }

        return game;
    }

}
