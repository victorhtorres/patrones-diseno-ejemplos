package course.design_patterns.factory_method;

/**
 * Creador concreto (concrete creator).
 *
 * Ver la documentación de la clase ChessBoard para más detalles.
 *
 * @author Manuel Zapata
 */
class SolitaireBoard extends Board{

    public SolitaireBoard(String[] players) {
        super(players);
    }

    @Override
    Game createGame() {

        String[] players = getPlayers();
        String player = "Jugador";
        if(players != null && players.length > 0) {
            player = players[0];
        }

        SolitaireGame game = new SolitaireGame(player);
        return game;
    }

}
