package course.design_patterns.state;

import java.time.Duration;

/**
 * Clase que utiliza el contexto.
 *
 * @author Manuel Zapata
 */
class StateClient {

    public static void main(String[] args) {

        //Cuando se crea el contexto, se le asigna un estado inicial en el constructor.
        MicrowaveOven oven = new MicrowaveOven();

        /* Invocamos métodos para analizar cómo va cambiando el estado del contexto. Observa la consola luego de
         * ejecutar el programa. */

        Duration duration = Duration.ofMinutes(120);
        oven.start(duration);

        oven.openDoor();

        oven.stop();

        oven.openDoor();

    }

}
