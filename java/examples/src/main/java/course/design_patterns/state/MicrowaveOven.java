package course.design_patterns.state;

import java.time.Duration;

/**
 * Contexto (context).
 *
 * Es la clase cuyo estado interno puede cambiar.
 *
 * @author Manuel Zapata
 */
class MicrowaveOven {

    //En este atributo se guardará el estado actual.
    private State currentState;

    public MicrowaveOven() {
        /*
        * Asignamos el estado inicial. Además, le damos una referencia al objeto actual.
        * */
        currentState = new NotHeatingState(this);
    }

    public boolean start(Duration duration) {
        System.out.println("Estado actual: " + currentState.getClass().getSimpleName() + " - iniciar");

        //El contexto no sabe realmente qué hacer en este método. Por eso, pasa la solicitud al estado actual.
        boolean result = currentState.start(duration);
        return result;
    }

    public boolean stop() {
        System.out.println("Estado actual: " + currentState.getClass().getSimpleName() + " - detener");

        //El contexto pasa la solicitud para que el estado la resuelva.
        boolean result = currentState.stop();
        return result;
    }

    public boolean openDoor(){
        System.out.println("Estado actual: " + currentState.getClass().getSimpleName() + " - abrir puerta");

        //El contexto pasa la solicitud para que el estado la resuelva.
        boolean result = currentState.openDoor();
        return result;
    }

    /**
     * En esta implementación del contexto, serán los estados quienes definen cuál es el siguiente estado, y lo deben
     * hacer a través de este método.
     * */
    public void changeState(State newState) {
        currentState = newState;
    }

}
